# Polish Vehicle Registration Certificate decoder
# Dekoder Dowodu Rejestracyjnego Pojazdu

Polish Vehicle Registration Certificate decoder.
Dekoder Dowodu Rejestracyjnego Pojazdu.

It decodes data from Aztec2D barcode placed on last page of certificate. Aztec2D barcode have to be read using barcode scanner and then additionally decoded using firstly Base64 and then decompressed using NRV2E algorithm.

Dekoduje dane zawarte w kodzie kreskowym Aztec2D umieszczonym na ostatniej stronie dowodu. Kod Aztec2D musi zostać najpierw odczytany skanerem kodów kreskowych obsługującym kody 2D, a następnie zdekodowany przy użyciu Base64 oraz zdekompresowany algorytmem NRV2E.

NRV2E algorithm was created by Markus F.X.J. Oberhumer (<markus@oberhumer.com>) and shared as open source implementation in UCL library <http://www.oberhumer.com/opensource/ucl/>. This library is using .NET reimplementation of this algorithm (https://bitbucket.org/bsoja/nrv2e-csharp).

Twórcą algorytmu NRV2E jest Markus F.X.J. Oberhumer (<markus@oberhumer.com>), który udostępnił jego implementację jako open source w biblitece UCL <http://www.oberhumer.com/opensource/ucl/>. Ta biblioteka używa reimplementacji tego algorytmu w .NET (https://bitbucket.org/bsoja/nrv2e-csharp).

----

Copyright (c) 2018 Bartosz Soja bartosz.soja@gmail.com

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

----

Library is hosted on NuGet and there you can find latest build.
```
https://www.nuget.org/packages/PolishVehicleRegistrationCertificateDecoder/
```

----

To build:
```
dotnet restore src/PolishVehicleRegistrationCertificateDecoder.csproj
msbuild /p:Configuration=Release /t:Clean,Build src/PolishVehicleRegistrationCertificateDecoder.csproj
```

To create NuGet package:
```
nuget pack PolishVehicleRegistrationCertificateDecoder.nuspec
```

----

Example usage:

```
var decoder = new PolishVehicleRegistrationCertificateDecoder.Decoder();
var vehicleRegistrationInfo = decoder.Decode("wAMAANtYAA...");
```
