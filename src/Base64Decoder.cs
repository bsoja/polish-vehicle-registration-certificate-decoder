using System;

namespace PolishVehicleRegistrationCertificateDecoder
{
    internal class Base64Decoder
    {
        public byte[] Decode(string data)
        {
            if (string.IsNullOrEmpty(data)) return new byte[0];

            if (data.Length % 4 == 1)
            {
                data = data.Substring(0, data.Length - 1);
            }

            var bytes = Convert.FromBase64String(data);
            return bytes;
        }
    }
}